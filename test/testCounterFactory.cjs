const counterFactory = require('../counterFactory.cjs');

const counterFunction = counterFactory();

const incrementValue = counterFunction.increment();
console.log(incrementValue);

const decrementValue = counterFunction.increment();
console.log(decrementValue);

