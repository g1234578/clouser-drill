const cacheFunction = require("../cacheFunction.cjs");

function cb(value) {
    return (value+20);
}

const func = cacheFunction(cb);

const ans1 = func(1);
console.log(ans1);
const ans2 = func(2);
console.log(ans2);
const ans3 = func(3);
console.log(ans3);
const ans4 = func(1);
console.log(ans4);