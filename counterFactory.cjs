function counterFactory() {
  let counter = 0;

  return {
    increment: function () {
      try {
        return ++counter;
      } catch (error) {
        console.error("Error occurred during increment:", error.message);
        return null;
      }
    },

    decrement: function () {
      try {
        return --counter;
      } catch (error) {
        console.error("Error occurred during decrement:", error.message);
        return null;
      }
    },
  };
}

module.exports = counterFactory;
