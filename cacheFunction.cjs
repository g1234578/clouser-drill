// Should return a function that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invok
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
  const cache = {};

  return function (argument) {
    try {
      if (cache.hasOwnProperty(argument)) {
        //hasOwnProperty returns the key is present or not
        return cache[argument];
      } else {
        const result = cb(argument);
        cache[argument] = result;
        return result;
      }
    } catch (error) {
      console.error("Error occurred during increment:", error.message);
      return null;
    }
  };
}

module.exports = cacheFunction;
