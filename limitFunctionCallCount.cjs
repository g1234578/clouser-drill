// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
  let count = 0;

  return function () {
    try {
      if (count < n) {
        count++;
        return cb();
      } else {
        console.log("Limit exceeded");
        return null;
      }
    } catch (error) {
      console.error("Error occurred:", error.message);
      return null;
    }
  };
}

module.exports = limitFunctionCallCount;

